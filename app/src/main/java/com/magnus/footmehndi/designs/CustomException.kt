package com.magnus.footmehndi.designs

class CustomException(message: String) : Exception(message)
